package require Tk

wm geometry . +100+100

set oper {+}
set op1 {2}
set op2 {3}

entry .n1 -textvariable op1
entry .n2 -textvariable op2
frame .op

radiobutton .plus -value {+} -variable oper -text {plus}
radiobutton .minus -value {-} -variable oper -text {minus}

proc kalk { }
{
	global op1
	global op2
	global oper
	set res [calc $op1 $oper $op2]
	.upshot delete 0 end
	.upshot insert 0 $res
}

button .submit -text { XXX } -command kalk
entry .upshot 

grid .plus -in .op -column 1 -row 1
grid .minus -in .op -column 2 -row 1

grid .n1 -column 1 -row 1
grid .op -column 1 -row 2
grid .n2 -column 1 -row 3
grid .submit -column 1 -row 4
grid .upshot -column 1 -row 5
