SWIG = swig
CFLAGS = -ltk8.5 -ltcl8.5 -g

all: clean tkalc

tkalc: tkalc.o
	$(CC) $(CFLAGS) -o tkalc tkalc.o

tkalc.o: tkalc.c tkalc_wrap.c
	$(CC) $(CFLAGS) -c -o tkalc.o tkalc.c

tkalc_wrap.c: tkalc.i
	$(SWIG) -tcl tkalc.i

run: tkalc
	./tkalc

clean:
	rm -f tkalc tkalc.o tkalc_wrap.c
