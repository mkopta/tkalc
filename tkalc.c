#include <stdio.h>
#include <stdlib.h>
#include <tk.h>
#include <tcl.h>
#include "./tkalc_wrap.c"

Tcl_Interp *interp = NULL;

int calc(int a, char op, int b)
{
	switch (op) {
	case '+':
		a += b;
		break;
	case '-':
		a -= b;
		break;
	default:
		a += b;
		break;
	}
	return a;
}

void init_tcl(void)
{
	if ((interp = Tcl_CreateInterp()) == NULL) {
		fprintf(stderr, "Tcl_CreateInterp fail\n");
		exit(1);
	}
	if (Tcl_Init(interp) == TCL_ERROR) {
		fprintf(stderr, "Tcl_Init fail\n");
		exit(1);
	}
	Tcl_CreateObjCommand(interp, SWIG_prefix "calc", _wrap_calc,
			(ClientData) NULL, (Tcl_CmdDeleteProc *) NULL);
}

int main(int argc, char **argv)
{
	init_tcl();
	Tcl_EvalFile(interp, "./gui.tcl");
	Tk_MainLoop();
	return 0;
}
